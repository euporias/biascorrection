rm(list=ls())
library(WFRTools)
source(file = "./functions/infoGeneral.R")
source(file = "./functions/functionsGeneral.R")
source(file = "./functions/functionNetcdf2R.R")
source(file = "./functions/functionConvert.R")
source(file = "./functions/functionMakeSelection.R")

submitscript <- FALSE

if (submitscript) {
  members      <- c(1:15)
  initYears    <- c(1981:2010)
  targetMonths <- c(X:X)
  leadMonths   <- c(0:6)
  locName      <- 'X'
  resolution   <- 'X'
  obsPath      <- sprintf("../DATA/wfdei_rev5.0/%sdeg/%s/", resolution, locName)
  inPathNoBC   <- sprintf("../DATA/System4_seasonal_15_rev5.0/%sdeg/%s_noBC_biasformat", resolution, locName)
  inPathBC     <- sprintf("../DATA/System4_seasonal_15_rev5.0/%sdeg/%s_BC_biasformat", resolution, locName)
  outPath      <- sprintf("../Analysis_output/pics/checkBias_rev5.0")
} else {
  members      <- c(1:15)
  initYears    <- c(1981:2010)
  targetMonths <- c(1:12)
  leadMonths   <- c(0:6)
  #locName      <- "GHA"
  locName      <- "EU"
  resolution   <- "0.50"
  obsPath      <- sprintf("../DATA/wfdei_rev5.0/%sdeg/%s/", resolution, locName)
  inPathNoBC   <- sprintf("../DATA/System4_seasonal_15_rev5.0/%sdeg/%s_noBC_biasformat", resolution, locName)
  inPathBC     <- sprintf("../DATA/System4_seasonal_15_rev5.0/%sdeg/%s_BC_biasformat", resolution, locName)
  outPath      <- sprintf("../Analysis_output/pics/checkBias_rev5.0")
}

mask<-Netcdf2R(inFile = sprintf("../DATA/mask/mask_wfdei_%s_%s.nc4", locName, resolution), "mask")

variables<-names(variableInfo)


print("start")
for (variableName in variables) {
  dir.create(sprintf("%s/%s", outPath, variableName), recursive = TRUE, showWarnings = FALSE)
  for (targetMonth in targetMonths) {
    for (leadMonth in leadMonths) {
      targetSYear <- getInitTargetInfo( initYear = initYears[1], targetMonth = targetMonth, leadMonth = leadMonth )$targetYear
      targetEYear <- getInitTargetInfo( initYear = initYears[length(initYears)], targetMonth = targetMonth, leadMonth = leadMonth )$targetYear
      
      ######## OPEN FILES #####################
      noBCFile <- sprintf("%s/%s/%s_forcing_seas15_%s_noBC_E%02d-%02d_TAR%4d-%4d_%02d_LM%d.nc4",
                          inPathNoBC, variableName, variableName, locName, 
                          members[1], members[length(members)],
                          targetSYear, targetEYear,
                          targetMonth, leadMonth)
      RData_noBC <- Netcdf2R(noBCFile, variableName)
      
      BCFile <- sprintf("%s/%s/%s_forcing_seas15_%s_BC_E%02d-%02d_TAR%4d-%4d_%02d_LM%d.nc4",
                        inPathBC, variableName, variableName, locName, 
                        members[1], members[length(members)],
                        targetSYear, targetEYear,
                        targetMonth, leadMonth)
      RData_BC <- Netcdf2R(BCFile, variableName)
      
      if (variableName == "pr") {
        obsFile <- sprintf("%s/%s_wfdei_GPCC_%s_%4d-%4d_%02d.nc4",
                           obsPath, variableName, locName, 
                           initYears[1], initYears[length(initYears)],
                           targetMonth)
      } else {
        obsFile <- sprintf("%s/%s_wfdei_%s_%4d-%4d_%02d.nc4",
                           obsPath, variableName, locName, 
                           initYears[1], initYears[length(initYears)],
                           targetMonth)
      }
      RData_obs <- Netcdf2R(obsFile, variableName)
      #############################
      
      ## Make uniform and match the selections
      print("making subset of overlapping data...")
      dates1<-as.character(as.POSIXct(RData_obs$Dates$start, tz = "GMT"))
      dates2<-as.character(as.POSIXct(RData_noBC$Dates$start, tz = "GMT"))
      overlapping_dates<-as.Date(intersect(dates1, dates2))
      rm(dates2,dates1)
      RData_obs  <- subsetRData(RData_obs,  dates = overlapping_dates)
      RData_noBC <- subsetRData(RData_noBC, dates = overlapping_dates)
      RData_BC   <- subsetRData(RData_BC,   dates = overlapping_dates)

      ## Convert to right unit
      RData_obs  <- convert(RData_obs,  toUnit = variableInfo[[variableName]]$unitsEasy)
      RData_noBC <- convert(RData_noBC, toUnit = variableInfo[[variableName]]$unitsEasy)
      RData_BC   <- convert(RData_BC,   toUnit = variableInfo[[variableName]]$unitsEasy)

      ## Apply mask on Observed-data
        for (iTime in 1:length(RData_obs$Dates$start)) {
          RData_obs$Data[iTime,,]<-RData_obs$Data[iTime,,]*mask$Data[1,,]
        }

      ## Check-Correction:
      barLimits<-c(colorbarLimits[[variableName]]$minValue,colorbarLimits[[variableName]]$maxValue)
      barLimitsDiff<-c(colorbarLimits[[variableName]]$diffMin,colorbarLimits[[variableName]]$diffMax)
      print("obs_mean...")
      RData_obs_mean <-RData_obs;RData_obs_mean$Data<-apply(RData_obs$Data, c(2,3), mean)
      p_obs<-ggplotje(RData_obs_mean, title = paste0(variableInfo[[variableName]]$longName, " [", variableInfo[[variableName]]$unitsEasy, "]\nWFDEI (", month.name[targetMonth], ")"),barLimits = barLimits)
      #        p_obs<-ggplotje(RData_obs_mean, title = paste0(variableInfo[[variableName]]$longName, " ", variableInfo[[variableName]]$unitsEasy, "\nWFDEI (", month.name[targetMonth], ")"))
      print("BC_mean...")
      RData_BC_mean<-RData_BC;RData_BC_mean$Data<-apply(RData_BC$Data, c(1,3,4), mean)
      p_BC<-ggplotje(RData_BC_mean, title = paste0(variableInfo[[variableName]]$longName, " [", variableInfo[[variableName]]$unitsEasy, "]\nseas15_BC (", month.name[targetMonth], ", leadMonth:", leadMonth, ")"),barLimits = barLimits)
      #        p_BC<-ggplotje(RData_BC_mean, title = paste0(variableInfo[[variableName]]$longName, " ", variableInfo[[variableName]]$unitsEasy, "\nseas15_BC (", month.name[targetMonth], ", LM:", leadMonth, "/SM:", iStartMonth, "/S:", iSeason, ")"))
      print("noBC_mean...")
      RData_noBC_mean<-RData_noBC;RData_noBC_mean$Data<-apply(RData_noBC$Data, c(1,3,4), mean)
      p_noBC<-ggplotje(RData_noBC_mean, title = paste0(variableInfo[[variableName]]$longName, " [", variableInfo[[variableName]]$unitsEasy, "]\nseas15_noBC (", month.name[targetMonth], ", leadMonth:", leadMonth, ")"),barLimits = barLimits)
      #        p_noBC<-ggplotje(RData_noBC_mean, title = paste0(variableInfo[[variableName]]$longName, " ", variableInfo[[variableName]]$unitsEasy, "\nseas15_noBC (", month.name[targetMonth], ", LM:", leadMonth, "/SM:", iStartMonth, "/S:", iSeason, ")"))
      
      RData_BC_mean2<-RData_BC
      RData_BC_mean2$Data<-apply(RData_BC_mean$Data, c(2,3), mean)
      obsdiff<-RData_obs_mean
      obsdiff$Data<-RData_obs_mean$Data-RData_BC_mean2$Data
      barmin<-min(obsdiff$Data, na.rm = TRUE)
      barmax<-max(obsdiff$Data, na.rm = TRUE)
      #       p_obsVsBC<-ggplotje(obsdiff, title = "Observed vs BC")
#      p_obsVsBC<-ggplotje(obsdiff, title = paste0(variableInfo[[variableName]]$longName, " ", variableInfo[[variableName]]$unitsEasy, "\nWFDEI - seas15_BC"),backgroudColor = "grey85")
      p_obsVsBC     <-ggplotje(obsdiff, title = paste0(variableInfo[[variableName]]$longName, " [", variableInfo[[variableName]]$unitsEasy, "]\nWFDEI - seas15_BC"),barLimits = c(barmin,barmax),barDiff=TRUE,backgroudColor = "grey85")
      p_obsVsBCbars <-ggplotje(obsdiff, title = paste0(variableInfo[[variableName]]$longName, " [", variableInfo[[variableName]]$unitsEasy, "]\nWFDEI - seas15_BC"),barLimits = barLimitsDiff,barDiff=TRUE,backgroudColor = "grey85")
      
      RData_noBC_mean2<-RData_BC
      RData_noBC_mean2$Data<-apply(RData_noBC_mean$Data, c(2,3), mean)
      obsdiffnoBC<-RData_obs_mean
      obsdiffnoBC$Data<-RData_obs_mean$Data-RData_noBC_mean2$Data
      #          p_obsVsnoBC<-ggplotje(obsdiffnoBC, title = "Observed vs noBC")
      #p_obsVsnoBC<-ggplotje(obsdiffnoBC, title = paste0(variableInfo[[variableName]]$longName, " ", variableInfo[[variableName]]$unitsEasy, "\nWFDEI - seas15_noBC"),backgroudColor = "grey85")
      barmin<-min(obsdiffnoBC$Data, na.rm = TRUE)
      barmax<-max(obsdiffnoBC$Data, na.rm = TRUE)
      p_obsVsnoBC     <-ggplotje(obsdiffnoBC, title = paste0(variableInfo[[variableName]]$longName, " [", variableInfo[[variableName]]$unitsEasy, "]\nWFDEI - seas15_noBC"),barLimits = c(barmin,barmax),barDiff=TRUE,backgroudColor = "grey85")
      p_obsVsnoBCbars <-ggplotje(obsdiffnoBC, title = paste0(variableInfo[[variableName]]$longName, " [", variableInfo[[variableName]]$unitsEasy, "]\nWFDEI - seas15_noBC"),barLimits = barLimitsDiff,barDiff=TRUE,backgroudColor = "grey85")
      
      print("densPlots...")
      densPlot <-function(lon = NULL, lat = NULL) {
        data_obs<-RData_obs$Data[,lat==RData_obs$xyCoords$y,lon==RData_obs$xyCoords$x]
        data_BC<-RData_BC$Data[members[1]:members[length(members)],,lat==RData_obs$xyCoords$y,lon==RData_obs$xyCoords$x]
        data_noBC<-RData_noBC$Data[members[1]:members[length(members)],,lat==RData_obs$xyCoords$y,lon==RData_obs$xyCoords$x]

        dat <- data.frame(legend = factor(c(rep("1: OBS",length(as.vector(data_obs))),
                                            rep("3: noBC",length(as.vector(data_noBC))),
                                            rep("2: BC",length(as.vector(data_BC))))), 
                          data = c(as.vector(data_obs),
                                   as.vector(data_noBC),
                                   as.vector(data_BC)))
        nBins<-20
        bins<-seq(min(dat$data),max(dat$data),(max(dat$data)-min(dat$data))/(nBins+1))
        # Density plots
        #  pplot<-ggplot(dat, aes(x=data, colour=legend, fill=legend)) + 
        pplot<-ggplot(dat, aes(x=data, fill=legend)) + 
          #geom_histogram(binwidth=1, position="dodge",aes(y = ..density..)) +
          geom_histogram(breaks=bins, position="dodge",aes(y = ..density..)) +
          #geom_density() +
          ggtitle(paste0("lon: ",lon, " lat: ", lat))
        return(pplot)
      }
      
      if (locName == "GHA") {
        p4<-densPlot(30.75,-2.25)
        p5<-densPlot(36.75,-2.25)
        #p6<-densPlot(36.75,11.25)
      } else if (locName == "EU") {
        p4<-densPlot(19.25,54.25)
        p5<-densPlot(27.25,43.75)
        #p6<-densPlot(10.25,46.25)
      } else {
        p4<-densPlot(-55.25,-10.25)
        p5<-densPlot(-60.25,-25.75)
      }
      
      nameFilePicture <- sprintf("%s/%s/%s_%s_%s_E%02d-%02d_TAR%4d-%4d_%02d_LM%d",
                                 outPath, variableName, variableName, locName, resolution, 
                                 members[1], members[length(members)],
                                 initYears[1], initYears[length(initYears)],
                                 targetMonth, leadMonth)
      
      print(paste0("Saving picture : ", nameFilePicture))
      
      png(paste0(nameFilePicture,'.png'), width = 1920, height = 1080)
#      multiplot(p_obs, p_noBC, p_BC, p_obs, p_obsVsnoBC, p_obsVsBC, p4 ,p5, p6, cols=3)
      multiplot(p_obs, p4, p5, p_noBC, p_obsVsnoBC, p_obsVsnoBCbars, p_BC, p_obsVsBC, p_obsVsBCbars, cols=3)
      
      dev.off()
    }
  }
}
