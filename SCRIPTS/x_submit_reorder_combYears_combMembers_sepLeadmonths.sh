#!/bin/bash
tmpScripts="./tmpScripts"
#location="EU";  resolution="0.50"
#location="GHA"; resolution="0.50"
#location="GHA"; resolution="0.75"
location="SA"; resolution="0.50"

bcInfo="noBC"
#bcInfo="BC"

mkdir -p $tmpScripts

#for iInitMonth in $(seq -f "%02g" 1 12); do
for iInitMonth in $(seq -f "%02g" 1 1); do
  echo "leadMonth: $iInitMonth"
  sed -e "s|<initMonth>|$iInitMonth|g" \
      -e "s|<location>|$location|g" \
      -e "s|<resolution>|$resolution|g" \
      -e "s|<bcInfo>|$bcInfo|g" \
      2_jobScriptReorder_combYears_combMembers_sepLeadmonths > $tmpScripts"/2_jobScriptReorder_combYears_combMembers_sepLeadmonths_"$location"_"$bcInfo"_"$resolution"deg_initMonth"$iInitMonth
  sed -e "s|submitscript <- FALSE|submitscript <- TRUE|g" \
      -e "s|initMonths   <- c(X:X)|initMonths   <- c($iInitMonth:$iInitMonth)|g" \
      -e "s|locName      <- 'X'|locName      <- '$location'|g" \
      -e "s|resolution   <- 'X'|resolution   <- '$resolution'|g" \
      -e "s|bcInfo       <- 'X'|bcInfo       <- '$bcInfo'|g" \
      2_doReorder_combYears_combMembers_sepLeadmonths.R > $tmpScripts"/2_doReorder_combYears_combMembers_sepLeadmonths_"$location"_"$bcInfo"_"$resolution"deg_initMonth"$iInitMonth".R"
  sbatch "$tmpScripts/2_jobScriptReorder_combYears_combMembers_sepLeadmonths_"$location"_"$bcInfo"_"$resolution"deg_initMonth"$iInitMonth
done
