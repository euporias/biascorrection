biasCorrectCell<-function(obscell, predcell, minobs, npred, nbin = 100, epsilon = 0.00001){
  # Due to set.seed, the same random numbers are generated each time
  #    the routine is run
  set.seed (423)
  rannumdis <- runif(npred, -0.5, 0.5)	     
  set.seed (187)
  rannum0   <- runif(npred,  0.0, 1.0)	     
  
  # To avoid that non-zero values occur more than once
  indab0 <- which (predcell > 0.0)
  predcell[indab0] <- predcell[indab0] + epsilon * rannumdis[indab0]
  
  # Precipitation cannot be less than 0   
  indbel0 <- which (predcell < 0.0)
  predcell[indbel0] = 0.0
  
  # The array with the corrected hindcasts gets the same structure as the
  #    hindcasts  
  simcell <- predcell
  
  # Determine the indices of the valid observations and hindcasts    
  indvalobs <- which (!is.na (obscell) == T)
  indvalpred <- which (!is.na (obscell) == T)
  
  # Go to next cell if there are no valid observations and hindcasts 
  if (length (indvalobs) == 0) return(simcell)
  if (length (indvalpred) == 0) return(simcell)
  
  if (length (indvalobs) < minobs) stop ("Less observations than expected")
  
  # Select valid observations and the corresponding predictions
  obsval  = obscell[indvalobs]   
  predval = predcell[indvalobs]
  if (sum (is.na (predval)) > 0) stop ("NaN in hindcasts")
  
  # Set bin properties, once per data set
  nobs0 = length(obsval)
  # Boundaries of the bins (counts)
  # bounnsam can be a non-integer
  # ibts and iets are indices of that delimit the bins containing the
  #    sorted observations and hindcasts
  # nsambin is the number of samples per bin   
  bounnsam <- seq(0, nobs0, length.out = nbin + 1)
  ibts <- floor(bounnsam[1:nbin]   + 1)
  iets <- floor(bounnsam[2:(nbin+1)])
  nsambin <- iets - ibts + 1
  meanobsbin  <- vector ("double", nbin)
  meanpredbin <- vector ("double", nbin)
  thrpredbin  <- vector ("double", nbin+1)
  thrpredbin[1]      = -999.0e8
  thrpredbin[nbin+1] = +999.0e8
  
  nobscell = length(obsval)
  if (nobscell != nobs0) stop ("number of observations differs between cells")
  
  # Sort the observations and the predictions   
  obssort  <- sort (obsval)
  predsort <- sort (predval)
  
  # Compute bin mean values, the correction for each bin and
  #    the correction for each bin   
  for (ibin in 1:nbin) {
    obsbin = obssort [ibts[ibin]:iets[ibin]]
    meanobsbin[ibin] = mean(obsbin)
    predbin = predsort [ibts[ibin]:iets[ibin]]
    meanpredbin[ibin] = mean(predbin)
    if (ibin != nbin) thrpredbin[ibin+1] = 0.5 * (predsort[iets[ibin]] + 
                                                    predsort[iets[ibin] + 1])
  }
  diffbin = meanobsbin - meanpredbin
  
  # Number of bins with no precipitation
  nbins0obs  <- length (which (meanobsbin == 0))
  nbins0pred <- length (which (meanpredbin == 0))
  morepred0 = F
  
  # If there are more bins with zero precipitation in the hindcasts
  #    than in the observations (morepred0 = T), there is an issue,
  #    which solution is prepared here
  # allcumsum0bins is the cumulative number of zeros 
  #    over the bins containing zeros (in the hindcasts)
  # thrattrrandom is the same as allcumsum0bins but rescaled to {0,1]
  if (nbins0pred > nbins0obs) {
    morepred0 = T 
    nsam0bins = nsambin[1:nbins0pred]
    cumsumsam0bins <- cumsum (nsam0bins)
    n0transbin = 0
    if (nbins0pred != nbin) {
      itrbin = nbins0pred + 1
      n0transbin <- length (which (predsort [ibts[itrbin]:iets[itrbin]] == 0))
    }
    n0tot <- sum (nsam0bins) + n0transbin  
    allcumsum0bins <- c (cumsumsam0bins, n0tot)
    thrattrrandom <- 1.0 * allcumsum0bins / n0tot
  }
  
  irn = 0
  
  # Correct simulated values (simcell)      
  for (isam in (1:npred)) {
    
    # Take the uncorrected value
    predsam = predcell[isam]
    
    # Skip NaN   
    if ( is.na(predsam)) {
      simcell[isam] = NaN
      next
    } 
    
    # Determine bin number for the special cases (morepred0 = T)
    #    and simulated zero values
    if (morepred0 & predsam == 0) {
      irn = irn + 1 
      rnhere = rannum0[irn]
      diffthr = thrattrrandom - rnhere
      indbinpos <- which(diffthr >= 0.0)		  
      ibinsel <- indbinpos[1]
    }
    
    # Determine bin number for all other cases (probably most cases)	  
    else {	
      diffthr = thrpredbin - predsam
      indbinpos <- which(diffthr >= 0.0)		  
      ibinsel <- indbinpos[1] - 1
    }
    
    # Apply correction and keep hold of results per bin for controlling
    #    the routine 
    simcell[isam] = predcell[isam] + diffbin[ibinsel]
    # nsimbin[ibinsel] = nsimbin[ibinsel] + 1 
    # sumsimbin[ibinsel] = sumsimbin[ibinsel] + simcell[isam]
  }
  
  # Precipitation cannot be less than 0   
  indbel0 <- which (simcell < 0.0)
  simcell[indbel0] = 0.0
  
  # Checks 
  # meansimbin = sumsimbin / nsimbin
  diffmean = abs (mean (simcell [indvalobs]) - mean (obscell [indvalobs]))
  # diffbinmax  = max (abs (meansimbin - meanobsbin), na.rm = T) 	
  if ((diffmean*86400) > 0.01 ) {
    print (paste ("error: ", diffmean, morepred0, length(indbel0), sep = "   "))
  }
  return(simcell)
}

biasCorrect <- function(obs, pred, nbin = 100, epsilon = 0.00001) {
  ## Make uniform and match the selections
  dates1<-as.character(as.POSIXct(obs$Dates$start, tz = "GMT"))
  dates2<-as.character(as.POSIXct(pred$Dates$start, tz = "GMT"))
  overlapping_dates<-as.Date(intersect(dates1, dates2))
  element<-is.element(dates2,dates1)
  rm(dates2,dates1)
  
  obs_new<-obs
  ntime<-dim(pred$Data)[2]
  nlat<-dim(obs$Data)[2]
  nlon<-dim(obs$Data)[3]
  obs_new$Data <- array(NA, dim = c(ntime, nlat, nlon))
  obs_new$Data[which(element==TRUE),,]<-obs$Data[]
  # obs_new$Data[,10,10]
  # obs$Data[,10,10]
  obs<-obs_new
  #############

  obssel <- obs$Data
  predsel <- pred$Data 
  RData_BC <- pred
  
  dimpred <- dim(predsel)
  nmem <- dimpred[1]
  nsam <- dimpred[2]
  nlat <- dimpred[3]
  nlon <- dimpred[4]
  npred = nmem * nsam
  
  obsselmem <- array(NA, dim = c(npred, nlat, nlon))
  
  for (imem in (1:nmem)) {
    ib = (imem - 1) * nsam + 1
    ie = ib + nsam - 1
    obsselmem[ib:ie, , ] <- obssel 
  }
  
  ########## START
  minobs = 28 * 29
  minobs = minobs * nmem
  
  for (ilon in (1:nlon)) {
    #ilon<-30
    print (paste (ilon, Sys.time(), sep = "   "))
    for (ilat in (1:nlat)) {
      #ilat<-40
      
      # Selection of all data for a single cell	
      obscell  <- obsselmem[ ,ilat, ilon]
      predcell <- aperm( predsel[ , ,ilat, ilon])
      rm(ib,ie,imem,obssel)
      simcell <- biasCorrectCell(obscell, predcell, npred = npred, minobs = minobs, nbin = nbin, epsilon = epsilon)
      # transpose back to the right member/timestep order
      dim(simcell) <- c(nsam, nmem)
      simcell<-t(simcell)
      RData_BC$Data[,,ilat,ilon]<-simcell
    }
  }
  return(RData_BC)
}
