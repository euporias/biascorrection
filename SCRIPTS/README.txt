## Step 1a (If downloading new data)
download the 0.75 degree data
download and regrid the 0.50 degree data
NOTE1: downloading combined years, combined members, seperate leadmonths gives strange timings in with the variables "tp", "rsds" and "rlds"
       it is better to download leadmonth0 and the season 1till7 in one go.
NOTE2: even if you download the correct way (see NOTE1), you should remove timestep 1 of leadmonth0 from "rsds" and "rlds" 
NOTE3: even if you download the correct way (see NOTE1), you should move all data one timestep backwards for "pr" (because t1 are all zero)

## Step 1b (If using old data)
reorder the old R-data to the Netcdf format.
the reordering script:
* selects the right region if needed (based on the mask file)
* applies a landsea mask (based on the mask file)
* removes values lower than zero (only for precipitation)
* converts to ALMA (model) units.

## Step 2
converts from:
* seperate years, seperate members, combined leadmonths 
to:
* combined years, combined members, seperate leadmonths

## Step 3
biascorrection
all datasets are: combined years, combined members, seperate leadmonths
