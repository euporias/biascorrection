#!/bin/bash
tmpScripts="./tmpScripts"
#location="EU";  resolution="0.50"
#location="GHA"; resolution="0.50"
#location="GHA"; resolution="0.75"
location="SA"; resolution="0.50"

mkdir -p $tmpScripts

#for iLeadMonth in $(seq -f "%02g" 0 0); do
for iLeadMonth in $(seq -f "%02g" 0 6); do
  echo "leadMonth: $iLeadMonth"
  sed -e "s|<leadMonth>|$iLeadMonth|g" \
      -e "s|<location>|$location|g" \
      -e "s|<resolution>|$resolution|g" \
      2_jobScriptFixRAWData > $tmpScripts"/2_jobScriptFixRAWData_"$location"_"$resolution"deg_leadMonth"$iLeadMonth
  sed -e "s|submitscript <- FALSE|submitscript <- TRUE|g" \
      -e "s|leadMonths  <- c(X:X)|leadMonths  <- c($iLeadMonth:$iLeadMonth)|g" \
      -e "s|locName    <- 'X'|locName    <- '$location'|g" \
      -e "s|resolution   <- 'X'|resolution   <- '$resolution'|g" \
      2_doFixRAWData.R > $tmpScripts"/2_doFixRAWData_"$location"_"$resolution"deg_leadMonth"$iLeadMonth".R"
  sbatch $tmpScripts"/2_jobScriptFixRAWData_"$location"_"$resolution"deg_leadMonth"$iLeadMonth
done
