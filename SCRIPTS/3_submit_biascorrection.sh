#!/bin/bash
tmpScripts="./tmpScripts"
#location="EU";  resolution="0.50"
#location="GHA"; resolution="0.50"
#location="GHA"; resolution="0.75"
location="SA"; resolution="0.50"

mkdir -p $tmpScripts

for iTargetMonth in $(seq -f "%02g" 1 12); do
  echo "leadMonth: $iTargetMonth"
  sed -e "s|<targetMonth>|$iTargetMonth|g" \
      -e "s|<location>|$location|g" \
      -e "s|<resolution>|$resolution|g" \
      3_jobScriptBiascorrection > $tmpScripts"/3_jobScriptBiascorrection_"$location"_"$resolution"deg_targetMonth"$iTargetMonth
  sed -e "s|submitscript <- FALSE|submitscript <- TRUE|g" \
      -e "s|targetMonths <- c(X:X)|targetMonths  <- c($iTargetMonth:$iTargetMonth)|g" \
      -e "s|locName      <- 'X'|locName      <- '$location'|g" \
      -e "s|resolution   <- 'X'|resolution   <- '$resolution'|g" \
      3_doBiascorrection.R > $tmpScripts"/3_doBiascorrection_"$location"_"$resolution"deg_targetMonth"$iTargetMonth".R"
  sbatch "$tmpScripts/3_jobScriptBiascorrection_"$location"_"$resolution"deg_targetMonth"$iTargetMonth
done
