inPath="/home/WUR/frans004/L_BACKUP/PROJECTS/EUPORIAS/biascorrection/DATA/System4_seasonal_15_rev5.0/0.50deg/SA_BC_final/"
outPath="/home/WUR/frans004/L_DATA/PROJECT_DATA/EUPORIAS/FORCING_DATA/System4_seasonal_15_rev5.0/0.50deg/SA_BC/"
tmpPath="./tmp/"

mkdir -p $tmpPath

currPath=$(pwd)"/"

cd $inPath
for i in $(ls *.nc4); do 
  cdo sellonlatbox,-83.25,-32.25,-55.75,14.25 $i "$currPath$tmpPath/$i" 
done

cd $currPath$tmpPath
for i in $(ls *.nc4); do 
  nccopy -k 'netCDF-4 classic model' $i "$outPath/$i"
done

