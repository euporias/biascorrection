#!/bin/bash
tmpScripts="./tmpScripts"
#location="GHA"
#location="EU"
#location="SA"
#location="GLOBAL"
location="S_ASIA"
mkdir -p $tmpScripts

for iLeadMonth in $(seq -f "%02g" 0 0); do
#for iLeadMonth in $(seq -f "%02g" 0 6); do
  echo "leadMonth: $iLeadMonth"
  sed -e "s|<leadMonth>|$iLeadMonth|g" \
      -e "s|<location>|$location|g" \
      1a_jobScriptDownloadSeas15 > $tmpScripts"/jobScriptDownloadSeas15_"$location"_leadMonth"$iLeadMonth
  sed -e "s|submitscript <- FALSE|submitscript <- TRUE|g" \
      -e "s|leadMonths  <- c(X:X)|leadMonths  <- c($iLeadMonth:$iLeadMonth)|g" \
      -e "s|locName    <- 'X'|locName    <- '$location'|g" \
      1a_doDownloadSeas15.R > $tmpScripts"/doDownloadSeas15_"$location"_leadMonth"$iLeadMonth".R"
  sbatch "$tmpScripts/jobScriptDownloadSeas15_"$location"_leadMonth"$iLeadMonth
done
